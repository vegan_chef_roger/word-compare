#!/usr/bin/python

import requests
import json
import numpy as np
from nltk.stem.lancaster import LancasterStemmer


class WordCompare():

    def __init__(self):
        self.apiKey = "5ZwvgXE4FcimrKTc5gd7"
        self.apiRoot = "http://thesaurus.altervista.org/thesaurus/v1"
        self.language = "en_US"
        self.st = LancasterStemmer()
        self.recursionLimit = 10
        self.words = list()

    def getSynonymns(self, w1):
        words = list()
        queryString = "?word=" + w1 + "&language=" + self.language + "&key=" + self.apiKey + "&output=json"
        fullURL = self.apiRoot + queryString
        r = json.loads(requests.get(fullURL).text.replace("(generic term)", "").replace("(related term)", "").replace("|", ","))
        if "response" in r.keys():
            for dictionary in r["response"]:
                #print(dictionary)
                synlist = dictionary["list"]["synonyms"]
                for word in synlist.split(","):
                    if "(antonym)" not in word:
                        words.append(word.rstrip())
        words2 = list()
        for word in words:
            if word not in words2:
                words2.append(word)
        return words2

    def compareWords(self, w1, w2):
        #print("w1 " + w1)
        #print(self.st.stem(w1))
        #print("w2 " + w2)
        #print(self.st.stem(w2))
        if w1 == w2:
            return True
        elif self.st.stem(w1) == self.st.stem(w2):
            return True
        else:
            return False

    def rate(self, w1, w2, hops):
        #print(w1)
        #print(w2)
        if self.compareWords(w1, w2):
            #print("................match..............")
            return hops
        else:
            hops = hops + 1
            if hops <= self.recursionLimit:
                syns = self.getSynonymns(w1)
                #print(syns)
                for syn in syns:
                    if self.compareWords(syn, w2):
                        return hops + 1
                for syn in syns:
                    if syn not in self.words:
                        self.words.append(syn)
                        h1 = self.rate(syn, w2, hops)
                        if h1 is not None:
                            return h1

    # sigmoid function
    def nonlin(self, x, deriv=False):
        if(deriv==True):
            return x*(1-x)
        return 1/(1+np.exp(-x))



if __name__ == "__main__":
    wc = WordCompare()
    rating = wc.rate("test", "assessment", 0)
    rating = wc.nonlin(rating)
    print(rating)